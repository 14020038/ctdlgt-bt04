package opp3;

import java.util.Random;

/**
 * Created by Chung on 9/22/2015.
 */
public class MaxMin {


    private int min;
    private int max;

    public static void main(String[] args) {

        Random random = new Random();

        int soN = 20;
        int array[] = new int[soN];
        MaxMin maxMin = new MaxMin();

        for (int i = 0; i < soN; i++) {

            array[i] = random.nextInt(100);
        }

        maxMin.maxMin(soN, array);

        // in ra mang
        System.out.println("Mang : ");
        for (int i = 0; i<soN; i++){
            System.out.println(array[i]);
        }

        // in ra max min
        System.out.print("Max = ");
        System.out.println(maxMin.getMax());
        System.out.print("Min  = ");
        System.out.println(maxMin.getMin());


    }


    public MaxMin() {
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }


    public void maxMin(int soN, int a[]) {

        int max, min;
        int min1 = 0;
        int max1 = 0;
        int m = soN / 2;

        if (a[0] > a[1]) {
            max = a[0];
            min = a[1];
        } else {
            max = a[1];
            min = a[0];
        }

        for (int i = 1; i < m; i++) {

            if (a[2 * i - 1] >= a[2 * i]) {
                max1 = a[2 * i - 1];
                min1 = a[2 * i];
            } else {
                max1 = a[2 * i];
                min1 = a[2 * i - 1];
            }
            max = (max > max1) ? max : max1;
            min = (min < min1) ? min : min1;
        }
        if (soN == (2 * m + 1)) {

            max = (max > a[soN]) ? max : a[soN];
            min = (min < a[soN]) ? min : a[soN];
        }

        this.setMax(max);
        this.setMin(min);
    }

}
