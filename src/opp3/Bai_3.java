package opp3;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by Chung on 9/22/2015.
 */

public class Bai_3 {

    public static  int MAX_SIZE = 23;

    public static void main(String[] args) {

        Bai_3 bai_3 = new Bai_3();

        int a[] = new int[MAX_SIZE];
        Random random = new Random();

        for (int i = 0; i < MAX_SIZE; i++) {
            a[i] = random.nextInt(100);
        }

        for (int i = 0; i < MAX_SIZE; i++) {
            System.out.println(a[i]);
        }
        Arrays.sort(a);

        // tim kiem so 2 trong mang
        int result = bai_3.BinarySearch(2, a);
        System.out.println("Ket qua tim kiem");
        System.out.println(result);

    }

    public int BinarySearch(int value, int[] arr) {

        int left = 0, right = arr.length, mid;
        int ret = -1;
        while (left < right) {
            mid = (left + right) / 2;
            if (arr[mid] == value) {
                ret = mid;
                break;
            } else if (arr[mid] > value) {
                right = mid;
            } else {
                left = mid + 1;
            }
        }
        return ret;
    }

}