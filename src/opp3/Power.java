package opp3;

/**
 * Created by Chung on 9/22/2015.
 */
public class Power {


    public static void main(String[] args) {

        Power power = new Power();

        float a = power.power(2, 3);
        System.out.println(a + "\t");

    }

    public float power(float x, int n) {

        float tem = 0;
        float result = 0;

        if (n == 0) {
            result = 1;
        } else if (n == 1) {
            result = x;
        } else if (n > 1) {
            tem = power(x, n / 2);
            if (n % 2 != 0) result = tem * tem * x;
            if (n % 2 == 0) result = tem * tem;
        }

        return result;
    }
}
